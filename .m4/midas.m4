dnl -*- mode: autoconf -*-
dnl
dnl $Id: midas.m4,v 1.3 2005/03/21 21:42:21 rdm Exp $
dnl $Author: rdm $
dnl $Date: 2005/03/21 21:42:21 $
dnl
dnl Autoconf macro to check for existence or MIDAS on the system
dnl Synopsis:
dnl
dnl  MIDAS_PATH([MINIMUM-VERSION, [ACTION-IF-FOUND, [ACTION-IF-NOT-FOUND]]])
dnl
dnl Some examples:
dnl
dnl    MIDAS_PATH(3.03/05, , AC_MSG_ERROR(Your MIDAS version is too old))
dnl    MIDAS_PATH(, AC_DEFINE([HAVE_MIDAS]))
dnl
dnl The macro defines the following substitution variables
dnl
dnl    MIDASEXEC           full path to midas
dnl    MIDASLIBDIR         Where the MIDAS libraries are
dnl    MIDASINCDIR         Where the MIDAS headers are
dnl    MIDASETCDIR         Where the MIDAS configuration is
dnl    MIDASCFLAGS         Extra compiler flags
dnl    MIDASLIBS           MIDAS basic libraries
dnl    MIDASGLIBS          MIDAS basic + GUI libraries
dnl    MIDASAUXLIBS        Auxilary libraries and linker flags for MIDAS
dnl    MIDASAUXCFLAGS      Auxilary compiler flags
dnl    MIDASRPATH          Same as MIDASLIBDIR
dnl
dnl The macro will fail if midas-config and midascint isn't found.
dnl
dnl Christian Holm Christensen <cholm@nbi.dk>
dnl
AC_DEFUN([MIDAS_PATH],
[
	AC_ARG_VAR([MIDASSYS], [top of the MIDAS installation directory])
	AC_ARG_WITH([midas],
	            [AC_HELP_STRING([--with-midas=DIR],
	                            [top of the MIDAS installation directory])],
	            [user_midassys=$withval],
	            [user_midassys="none"])
	if test x"$user_midassys" = xno; then
		no_midas="yes"
	else
		if test ! x"$user_midassys" = xnone; then
			midasbin="$user_midassys/bin"
		elif test ! x"$MIDASSYS" = x ; then
			midasbin="$MIDASSYS/bin"
		else
			midasbin=$PATH
		fi

		AC_PATH_PROG(MIDASPROG, mhttpd, no, $midasbin)

		if test "x$MIDASPROG" = xno; then
			no_midas="yes"
		else

			MIDASBIN=`AS_DIRNAME(["$MIDASPROG"])`
			MIDASSYS=`AS_DIRNAME(["$MIDASBIN"])`

			MIDASLIB=$MIDASSYS/lib
			MIDASINC=$MIDASSYS/include

			SAVE_CPPFLAGS=$CPPFLAGS
			CPPFLAGS="$CPPFLAGS -I$MIDASINC"
			AC_CHECK_HEADER([midas.h], , [no_midas="yes"
			                              AC_MSG_WARN([midas.h not found in $MIDASINC])])
			CPPFLAGS=$SAVE_CPPFLAGS

			SAVE_LDFLAGS=$LDFLAGS
			LDFLAGS="$LDFLAGS -L$MIDASLIB"
			AC_CHECK_LIB([midas], [hs_dump], [noop="noop"], [no_midas="yes"
			                                                 AC_MSG_WARN([libmidas not found in $MIDASLIB])], [-pthread -lrt -lutil])
			LDFLAGS=$SAVE_LDFLAGS

			AC_SUBST(MIDASLIB)
			AC_SUBST(MIDASINC)
			AC_SUBST(MIDASBIN)
			AC_SUBST(MIDASSYS)
		fi
	fi

	if test "x$no_midas" = "x" ; then
		ifelse([$1], , :, [$1])
	else
		ifelse([$2], , :, [$2])
	fi
])
